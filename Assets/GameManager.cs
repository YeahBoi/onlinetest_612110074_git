﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject GreenPlayerPrefab;
    public GameObject YellowPlayerPrefab;
    public GameObject pauseCanvas;
    public bool paused = false;
    public Text messageText;



    public static GameManager instance;
    public static int greenScore = 0;
    public static int yellowScore = 0;
    public Text greenScoreText;
    public Text yellowScoreText;

    public static readonly byte RestartGameEventCode = 1;

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(DisplayMessage(" KILL THEM ! ! "));

        SetPaused();

        
        if (NetworkPlayerManager.localPlayerInstance == null)
        {
            
            int team = (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
            Debug.Log($"Team number {team} is being instantiated");
            
            if (team == 0)
            {
                
                Transform spawn = SpawnManager.instance.GetTeamSpawn(0);
                PhotonNetwork.Instantiate(GreenPlayerPrefab.name, spawn.position, spawn.rotation);
            }
            else
            {
                
                Transform spawn = SpawnManager.instance.GetTeamSpawn(1);
                PhotonNetwork.Instantiate(YellowPlayerPrefab.name, spawn.position, spawn.rotation);
            }

        }

    }

    IEnumerator DisplayMessage(string message)
    {
        messageText.text = message;
        yield return new WaitForSeconds(2);
        messageText.text = "";
    }

    private void Awake()
    {
        SetScoresText();
        instance = this;
    }

    void SetScoresText()
    {
        greenScoreText.text = "GreenScore : " + greenScore.ToString();
        yellowScoreText.text ="YellowScore : " + yellowScore.ToString();
    }

    public void Quit()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("pause?");
            paused = !paused;
            SetPaused();
        }
        SetScoresText();

        if (greenScore >= 5)
        {
            StartCoroutine(DisplayMessage("Green Team Wins"));
            if (PhotonNetwork.IsMasterClient)
            {
                StartCoroutine(RestartGame());
            }
        }
        if (yellowScore >= 5)
        {
            StartCoroutine(DisplayMessage("Yellow Team Wins"));
            if (PhotonNetwork.IsMasterClient)
            {
               StartCoroutine(RestartGame());
            }
        }

    }

    IEnumerator RestartGame()
    {
        greenScore = 0;
        yellowScore = 0;
        yield return new WaitForSeconds(2);
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        ExitGames.Client.Photon.SendOptions sendOptions = new ExitGames.Client.Photon.SendOptions { Reliability = true };
        PhotonNetwork.RaiseEvent(RestartGameEventCode,null,raiseEventOptions, sendOptions);
    }

    void SetPaused()
    {
        pauseCanvas.SetActive(paused);
        Cursor.lockState = paused ?CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = paused;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        ReloadLevel();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        ReloadLevel();
    }

    public void ReloadLevel()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(1);
        }
    }


}
