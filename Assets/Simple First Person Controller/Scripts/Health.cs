﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class Health : MonoBehaviourPunCallbacks, IPunObservable, IOnEventCallback
{

    Renderer[] visuals;
    public int health = 100;
    

    int team = 0;

    public void OnGUI()
    {
        if (photonView.IsMine)
            GUI.Label(new Rect(750, 100, 1000, 1000), "Health : " + health);
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(health);
        }
        else
        {
            health = (int)stream.ReceiveNext();
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        
    }

    IEnumerator Respawn()
    {
        //SetRenderers(false);
        health = 100;
        GetComponent<CharacterController>().enabled = false;
        Transform spawn = SpawnManager.instance.GetTeamSpawn(team);
        transform.position = spawn.position;
        transform.rotation = spawn.rotation;
        GetComponent<CharacterController>().enabled = true;
        yield return new WaitForSeconds(1);
        //SetRenderers(true);
    }

    //void SetRenderers(bool state)
    //{
    //    foreach (var renderer in visuals)
    //    {
    //        renderer.enabled = state;
    //    }
    //}

    // Start is called before the first frame update
    void Start()
    {
        visuals = GetComponentsInChildren<Renderer>();
        team = (int)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            if(photonView.IsMine)
            {
                photonView.RPC("RPC_PlayerKilled", RpcTarget.All, team);
                StartCoroutine(Respawn());
            }
            
        }
    }

    [PunRPC]
    void RPC_PlayerKilled(int team)
    {
        if (team == 0)
        {
            GameManager.yellowScore++;
            //Debug.Log("green + 1");
        }

        else
        {
            GameManager.greenScore++;
            //Debug.Log("yellow + 1");
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == GameManager.RestartGameEventCode)
        {
            GameManager.greenScore = 0;
            GameManager.yellowScore = 0;
            health = 100;
            StartCoroutine(Respawn());
        }
    }

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
}
