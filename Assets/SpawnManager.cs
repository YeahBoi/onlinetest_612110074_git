﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    
    public static SpawnManager instance;
    
    GameObject[] greenTeamSpawns;
    GameObject[] yellowTeamSpawns;

    
    void Awake()
    {
        
        instance = this;
        greenTeamSpawns = GameObject.FindGameObjectsWithTag("GreenSpawn");
        yellowTeamSpawns = GameObject.FindGameObjectsWithTag("YellowSpawn");
    }

    public Transform GetRandomGreenSpawn()
    {
        
        return greenTeamSpawns[Random.Range(0, greenTeamSpawns.Length)].transform;
    }

    public Transform GetRandomYellowSpawn()
    {
        
        return yellowTeamSpawns[Random.Range(0, yellowTeamSpawns.Length)].transform;
    }
    
    public Transform GetTeamSpawn(int teamNumber)
    {
        return teamNumber == 0 ? GetRandomGreenSpawn() : GetRandomYellowSpawn();
    }


}
